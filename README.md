# TYPO3 Bootstrap Minimal

This is a TYPO3 extension providing the setup to render the entire page via fluid using 
bootstrap markup only. It is not a standalone integration but supposed to be included and
extended by a *site package*.

## Features

**Responsive picture sets**

All (text + ) image elements have crop variants for different displays / devices and are 
provided in multiple resolutions using the <picture> tag and the srcset attribute.

**Fluid-only HTML generation**

From the doctype to the closing html tag, no Typoscript rendering. This gives much greater
control over the generated HTML and making changes easier, though this also means that 
includeCSS/JS in the page cObject will have no effect.

**Bootstrap-only classes and nesting**

All default content elements are rendered with bootstrap classes. eg.: the textpic and textmedia
elements use the bootstrap grid to display the columns of text and pictures defined by the 
elements options.

## Usage

Run `composer require grossberger-georg/bootstrap-minimal`. Then use a site package extension
to include the TypoScript and TSConfig into the installation. See the extension at `src/ext/theme` 
in the repository [typo3-init](https://github.com/garfieldius/typo3-init) as an example.

## License

[MIT](https://opensource.org/licenses/MIT)
