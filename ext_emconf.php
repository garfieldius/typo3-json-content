<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

$EM_CONF[$_EXTKEY] = [
    'title'            => 'JSON Content',
    'description'      => 'Fully integrated JSON only frontend for TYPO3',
    'version'          => '1.0.0',
    'state'            => 'stable',
    'category'         => 'fe',
    'clearCacheOnLoad' => 0,
    'constraints'      => [
        'depends' => [
            'extbase' => '',
            'fluid'   => '',
        ],
        'conflicts' => [
            'fluid_styled_content' => '9.5.0-10.4.999',
        ],
        'suggests' => [],
    ],
];
