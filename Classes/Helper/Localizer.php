<?php
declare(strict_types=1);

namespace GrossbergerGeorg\JsonContent\Helper;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class Localizer
{
    public function translate(string $key, string $extensionName = ''): string
    {
        $result = $key;

        if ($key) {
            $translation = LocalizationUtility::translate($key, $extensionName);

            if ($translation) {
                $result = (string) $translation;
            }
        }

        return $result;
    }
}
