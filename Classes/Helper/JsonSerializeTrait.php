<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Helper;

use TYPO3\CMS\Core\Core\Environment;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
trait JsonSerializeTrait
{
    public function serialize($value): string
    {
        $opts = JSON_BIGINT_AS_STRING | JSON_THROW_ON_ERROR;

        if (Environment::getContext()->isDevelopment()) {
            $opts |= JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE;
        }

        return json_encode($value, $opts);
    }
}
