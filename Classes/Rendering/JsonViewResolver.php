<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\View\GenericViewResolver;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class JsonViewResolver extends GenericViewResolver
{
    public function resolve(string $controllerObjectName, string $actionName, string $format): ViewInterface
    {
        if (TYPO3_MODE === 'FE') {
            return GeneralUtility::makeInstance(InMemoryJsonView::class);
        }

        return parent::resolve($controllerObjectName, $actionName, $format);
    }
}
