<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use GrossbergerGeorg\BaseMinimal\CropVariants\GalleryBuilder;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference as ExtbaseReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractDomainObject;
use TYPO3\CMS\Extbase\Mvc\Controller\ControllerContext;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class InMemoryJsonView implements ViewInterface
{
    private ContentObjectRenderer $contentObjectRenderer;

    private GalleryBuilder $galleryBuilder;

    private string $table = '';

    private array $galleryFields = [];

    private static array $variables = [];

    /**
     * @param ContentObjectRenderer $contentObjectRenderer
     * @param GalleryBuilder $galleryBuilder
     */
    public function __construct(
        ContentObjectRenderer $contentObjectRenderer,
        GalleryBuilder $galleryBuilder
    ) {
        $this->contentObjectRenderer = $contentObjectRenderer;
        $this->galleryBuilder = $galleryBuilder;
    }

    /**
     * @param ContentObjectRenderer $contentObjectRenderer
     */
    public function injectContentObjectRenderer(ContentObjectRenderer $contentObjectRenderer): void
    {
        $this->contentObjectRenderer = $contentObjectRenderer;
    }

    /**
     * @param string $table
     */
    public function setTable(string $table): void
    {
        $this->table = $table;
        $this->galleryBuilder->setTable($table);
    }

    /**
     * @param string $galleryFields
     */
    public function setGalleryFields(string $galleryFields): void
    {
        $this->galleryFields = GeneralUtility::trimExplode(',', $galleryFields, true);
    }

    public function assign($key, $value)
    {
        self::$variables[$key] = $value;
    }

    public function assignMultiple(array $values)
    {
        ArrayUtility::mergeRecursiveWithOverrule(self::$variables, $values);
    }

    public function render()
    {
        return '';
    }

    public function setControllerContext(ControllerContext $controllerContext)
    {
    }

    public function canRender(ControllerContext $controllerContext)
    {
        return TYPO3_MODE === 'FE';
    }

    public function initializeView()
    {
    }

    public function toArray(string $namespace, int $pageSize = 8): array
    {
        $variables = self::$variables;
        $this->transformValues($variables, $namespace, $pageSize);
        return $variables;
    }

    public function reset(): void
    {
        self::$variables = [];
    }

    private function transformValues(array &$variables, string $namespace, int $itemsPerPage): void
    {
        foreach ($variables as $name => $value) {
            if ($value instanceof \JsonSerializable) {
                $variables[$name] = $value = $value->jsonSerialize();
            }

            if ($value instanceof QueryResultInterface) {
                $numberOfPages = MathUtility::forceIntegerInRange(ceil($value->count() / $itemsPerPage), 1, 9999);
                $currentPage = (int) (GeneralUtility::_GP($namespace)['@widget_0']['currentPage'] ?? 1);
                $currentPage = MathUtility::forceIntegerInRange($currentPage, 1, $numberOfPages);
                $value->getQuery()->setOffset(($currentPage - 1) * $itemsPerPage);
                $value->getQuery()->setLimit($itemsPerPage);
                $data = [
                    'total'       => $value->count(),
                    'pages'       => $numberOfPages,
                    'currentPage' => $currentPage,
                    'perPage'     => $itemsPerPage,
                    'urls'        => [],
                    'items'       => [],
                ];

                $links = [
                    'first' => 1,
                    'prev'  => $currentPage - 1,
                    'next'  => $currentPage + 1,
                    'last'  => $numberOfPages,
                ];

                foreach ($links as $type => $pageNum) {
                    $params = '';

                    if ($pageNum > 1) {
                        $params = '&' . $namespace . '[@widget_0][currentPage]=' . $pageNum;
                    }

                    $data['urls'][$type] = $this->contentObjectRenderer->typoLink_URL([
                        'parameter'        => $GLOBALS['TSFE']->id,
                        'additionalParams' => $params,
                    ]);
                }

                $data['items'] = $value->toArray();

                foreach ($this->galleryFields as $field) {
                    $this->galleryBuilder->setField($field);

                    foreach ($data['items'] as $index => $item) {
                        if ($item instanceof AbstractDomainObject) {
                            $data['items'][$index] = $item = $item->_getProperties();
                        }

                        if (!empty($item[$field])) {
                            $references = $item[$field];

                            if ($references instanceof ObjectStorage) {
                                $references = $references->toArray();

                                if (!$references) {
                                    $data['items'][$index][$field] = [];
                                    continue;
                                }
                            }

                            $data['items'][$index][$field] = $this->galleryBuilder->createList(
                                $data['items'][$index],
                                [],
                                $references
                            );
                        }
                    }
                }

                $variables[$name] = $value = $data;
            }

            if ($value instanceof \JsonSerializable) {
                $variables[$name] = $value = $value->jsonSerialize();
            }

            if ($value instanceof ObjectStorage) {
                $variables[$name] = $value = $value->toArray();
            }

            if ($value instanceof FileInterface) {
                $variables[$name] = $value = $value->getPublicUrl();
            }

            if (is_iterable($value) && !is_array($value)) {
                $variables[$name] = $value = iterator_to_array($value);
            }

            if ($value instanceof AbstractDomainObject) {
                $variables[$name] = $value = $value->_getProperties();
            }

            if ($value instanceof ExtbaseReference) {
                $variables[$name] = $value = $value->getOriginalResource();
            }

            if (is_array($value)) {
                $this->transformValues($variables[$name], $namespace, $itemsPerPage);
            }
        }
    }
}
