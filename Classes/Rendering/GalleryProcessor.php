<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use GrossbergerGeorg\BaseMinimal\CropVariants\GalleryBuilder;
use GrossbergerGeorg\BaseMinimal\CropVariants\Variant;
use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Resource\FileReference;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class GalleryProcessor implements DataProcessorInterface
{
    private GalleryBuilder $galleryBuilder;

    private LinkService $linkService;

    /**
     * @param LinkService $linkService
     * @param GalleryBuilder $galleryBuilder
     */
    public function __construct(LinkService $linkService = null, GalleryBuilder $galleryBuilder = null)
    {
        $this->linkService = $linkService ?? GeneralUtility::makeInstance(LinkService::class);
        $this->galleryBuilder = $galleryBuilder ?? GeneralUtility::makeInstance(GalleryBuilder::class);
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $this->galleryBuilder->setTable($processorConfiguration['table'] ?? 'tt_content');
        $this->galleryBuilder->setField($processorConfiguration['field'] ?? 'image');

        $gallery = $this->galleryBuilder->createList(
            $processedData,
            $processorConfiguration['rendererOptions'] ?? []
        );

        $mapper = function (array $item) use ($cObj) {
            foreach ($item as $key => $value) {
                if ($value instanceof FileReference) {
                    $link = null;

                    if ($value->getProperty('link')) {
                        $link = $cObj->typoLink_URL([
                            'parameter' => (string) $value->getProperty('link'),
                        ]);
                    }

                    $value = [
                        'url'           => $value->getPublicUrl(),
                        'title'         => (string) $value->getProperty('title'),
                        'description'   => (string) $value->getProperty('description'),
                        'alternative'   => (string) $value->getProperty('alternative'),
                        'link'          => $link,
                        'autoplay'      => (bool) $value->getProperty('autoplay'),
                        'showinpreview' => (int) $value->getProperty('showinpreview'),
                    ];
                } elseif ($value instanceof Variant) {
                    $value = $value->jsonSerialize();
                }
                $item[$key] = $value;
            }

            return $item;
        };

        $gallery = array_map($mapper->bindTo($this), $gallery);
        $as = $processorConfiguration['as'] ?? 'gallery';
        $processedData[$as] = $gallery;

        return $processedData;
    }
}
