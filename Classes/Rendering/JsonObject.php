<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use GrossbergerGeorg\JsonContent\Helper\JsonSerializeTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\AbstractContentObject;
use TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class JsonObject extends AbstractContentObject
{
    use JsonSerializeTrait;

    public function render($conf = [])
    {
        $processor = GeneralUtility::makeInstance(ContentDataProcessor::class);
        $data = $processor->process($this->cObj, ['dataProcessing.' => $conf], []);

        $this->removeInternalData($data);

        return $this->serialize($data);
    }

    private function removeInternalData(array &$data): void
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                if ($key === 'data') {
                    unset($data[$key]);
                } else {
                    $this->removeInternalData($data[$key]);
                }
            }
        }
    }
}
