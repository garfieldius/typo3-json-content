<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ContentProcessor implements DataProcessorInterface
{
    /**
     * @var ConnectionPool
     */
    private ConnectionPool $pool;

    /**
     * @var ContentDataProcessor
     */
    private ContentDataProcessor $processor;

    /**
     * ContentProcessor constructor.
     * @param ConnectionPool $pool
     * @param ContentDataProcessor $processor
     */
    public function __construct(ConnectionPool $pool = null, ContentDataProcessor $processor = null)
    {
        $this->pool = $pool ?? GeneralUtility::makeInstance(ConnectionPool::class);
        $this->processor = $processor ?? GeneralUtility::makeInstance(ContentDataProcessor::class);
    }

    /**
     * @param ContentObjectRenderer $cObj The data of the content element or page
     * @param array $contentObjectConfiguration The configuration of Content Object
     * @param array $processorConfiguration The configuration of this processor
     * @param array $processedData Key/value store of processed data (e.g. to be passed to a Fluid View)
     * @return array the processed data as key/value store
     */
    public function process(ContentObjectRenderer $cObj, array $contentObjectConfiguration, array $processorConfiguration, array $processedData): array
    {
        $as = $cObj->stdWrapValue('as', $processorConfiguration, 'content');
        $processedData[$as] = [];

        $removeFields = $processorConfiguration['removeFields'] ?? '';
        $removeFields = GeneralUtility::trimExplode(',', $removeFields, true);

        $qb = $this->pool->getQueryBuilderForTable('tt_content');
        $qb->select('*');
        $qb->from('tt_content');
        $qb->where(
            $qb->expr()->eq('pid', (int) $GLOBALS['TSFE']->id),
            $qb->expr()->eq('colPos', $processorConfiguration['colPos']),
        );
        $qb->addOrderBy('sorting', 'ASC');
        $stmt = $qb->execute();
        $rows = $stmt->fetchAll();
        $stmt->closeCursor();

        $transforms = $processorConfiguration['dataProcessing.'];

        foreach ($rows as $ce) {
            $cType = $ce['CType'];
            $listType = $ce['list_type'];
            $processings = $transforms[$listType . '.'] ?? $transforms[$cType . '.'] ?? $transforms['default.'] ?? [];

            if ($processings) {
                $ce = $this->processor->process($cObj, ['dataProcessing.' => $processings], $ce);
            }

            $processedData[$as][] = $this->stripFields($ce, $removeFields);
        }

        return $processedData;
    }

    private function stripFields(array $ce, array $removeFields): array
    {
        unset(
            $ce['rowDescription'],
            $ce['tstamp'],
            $ce['crdate'],
            $ce['cruser_id'],
            $ce['deleted'],
            $ce['hidden'],
            $ce['starttime'],
            $ce['endtime'],
            $ce['fe_group'],
            $ce['sorting'],
            $ce['editlock'],
            $ce['sys_language_uid']
        );

        static $removedPrefixes = [
            'l18',
            'l10',
            't3v',
            't3_',
        ];

        foreach (array_keys($ce) as $key) {
            $start = substr($key, 0, 3);

            if (in_array($start, $removedPrefixes)) {
                unset($ce[$key]);
            }
        }

        return $ce;
    }
}
