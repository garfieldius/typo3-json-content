<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use GrossbergerGeorg\JsonContent\Helper\Localizer;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentDataProcessor;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ExtbaseProcessor implements DataProcessorInterface
{
    /**
     * @var ContentDataProcessor
     */
    private ContentDataProcessor $processor;

    private FlashMessageService $flashMessageService;

    private Localizer $l10n;

    /**
     * @param FlashMessageService $flashMessageService
     * @param Localizer $l10n
     * @param ContentDataProcessor $processor
     */
    public function __construct(
        FlashMessageService $flashMessageService,
        Localizer $l10n,
        ContentDataProcessor $processor = null
    ) {
        $this->processor = $processor;
        $this->flashMessageService = $flashMessageService;
        $this->l10n = $l10n;
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $plugin = 'tx_' . strtolower(str_replace('_', '', $processorConfiguration['extension'])) . '.';
        $namespace = substr($plugin, 0, -1) . '_' . strtolower(str_replace('_', '', $processorConfiguration['plugin']));
        $setup = $GLOBALS['TSFE']->tmpl->setup['plugin.'][$plugin] ?? [];
        $config = [
            'userFunc'      => 'TYPO3\\CMS\\Extbase\\Core\\Bootstrap->run',
            'vendorName'    => $processorConfiguration['vendor'],
            'extensionName' => $processorConfiguration['extension'],
            'pluginName'    => $processorConfiguration['plugin'],
            'settings'      => $setup['settings.'] ?? [],
            'persistence'   => $setup['persistence.'] ?? [],
            'view'          => $setup['view.'] ?? [],
        ];

        $view = GeneralUtility::makeInstance(InMemoryJsonView::class);
        $view->reset();

        $renderer = clone $cObj;
        $renderer->data = $processedData;
        $renderer->cObjGetSingle('USER', $config);

        $view->setTable($processorConfiguration['table'] ?? 'tt_content');
        $view->setGalleryFields($processorConfiguration['galleryFields'] ?? 'image,media,falMedia,assets');

        $data = $view->toArray($namespace, (int) ($processorConfiguration['pageSize'] ?? 10));
        $view->reset();

        $as = $processorConfiguration['as'] ?? 'content';
        $removeKeys = GeneralUtility::trimExplode(',', $processorConfiguration['removeKeys'], true);

        foreach ($removeKeys as $key) {
            try {
                $data = ArrayUtility::removeByPath($data, $key);
            } catch (\Throwable $ex) {
                // Invalid paths are simply ignored
            }
        }

        foreach ($processorConfiguration['dataProcessing.'] ?? [] as $key => $config) {
            $path = substr($key, 0, -1);

            if (StringUtility::endsWith($key, '.') && ArrayUtility::isValidPath($data, $path, '.')) {
                $elements = ArrayUtility::getValueByPath($data, $path, '.');

                foreach ($elements as $index => $element) {
                    $elements[$index] = $this->processor->process(
                        $cObj,
                        ['dataProcessing.' => $config],
                        $element
                    );
                }

                $data = ArrayUtility::setValueByPath($data, $path, $elements, '.');
            }
        }
        $queue = $this->flashMessageService->getMessageQueueByIdentifier('extbase.flashmessages.' . $namespace);
        $data['messages'] = [];

        /** @var \TYPO3\CMS\Core\Messaging\FlashMessage $message */
        foreach ($queue->getAllMessagesAndFlush() as $message) {
            $data['messages'][] = [
                'message' => $this->l10n->translate((string)$message->getMessage()),
                'title' => $this->l10n->translate((string)$message->getTitle()),
                'severity' => $message->getSeverity(),
            ];
        }

        $processedData[$as] = $data;

        return $processedData;
    }
}
