<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class TypolinkProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $table = $processorConfiguration['table'] ?? $cObj->getCurrentTable();
        $cObj = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $cObj->start($processedData, $table);
        $as = $processorConfiguration['as'] ?? 'url';

        $processedData[$as] = $cObj->typoLink_URL($processorConfiguration['typolink.'] ?? []);

        return $processedData;
    }
}
