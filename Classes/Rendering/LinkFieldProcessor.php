<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\LinkHandling\LinkService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class LinkFieldProcessor implements DataProcessorInterface
{
    private LinkService $linkService;

    /**
     * TypolinkProcessor constructor.
     * @param LinkService $linkService
     */
    public function __construct(LinkService $linkService = null)
    {
        $this->linkService = $linkService ?? GeneralUtility::makeInstance(LinkService::class);
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $fields = $processorConfiguration['fields'] ?? '';

        foreach (GeneralUtility::trimExplode(',', $fields, true) as $field) {
            $value = $processedData[$field] ?? null;

            if ($value) {
                $processedData[$field] = $cObj->typoLink_URL(['parameter' => $value]);
            }
        }

        return $processedData;
    }
}
