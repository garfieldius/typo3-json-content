<?php
declare(strict_types=1);

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\Service\FlexFormService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class FlexformProcessor implements DataProcessorInterface
{
    private FlexFormService $flexformService;

    /**
     * FlexformProcessor constructor.
     * @param FlexFormService $flexformService
     */
    public function __construct(FlexFormService $flexformService)
    {
        $this->flexformService = $flexformService;
    }

    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $fields = $processorConfiguration['fields'] ?? 'pi_flexform';

        foreach (GeneralUtility::trimExplode(',', $fields, true) as $field) {
            $value = $processedData[$field] ?? null;

            if ($value && is_string($value)) {
                $value = $this->flexformService->convertFlexFormContentToArray($value);
            }

            $processedData[$field] = $value;
        }

        return $processedData;
    }
}
