<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Rendering;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\ContentObject\DataProcessorInterface;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class RichtextProcessor implements DataProcessorInterface
{
    public function process(
        ContentObjectRenderer $cObj,
        array $contentObjectConfiguration,
        array $processorConfiguration,
        array $processedData
    ) {
        $fields = $processorConfiguration['fields'] ?? 'bodytext';

        foreach (GeneralUtility::trimExplode(',', $fields) as $field) {
            $value = $processedData[$field] ?? null;

            if ($value) {
                $processedData[$field] = $cObj->parseFunc($value, [], '< lib.parseFunc_RTE');
            }
        }

        return $processedData;
    }
}
