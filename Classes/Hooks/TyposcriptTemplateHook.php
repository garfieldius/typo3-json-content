<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Hooks;

use TYPO3\CMS\Core\TypoScript\TemplateService;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class TyposcriptTemplateHook
{
    public function addFakeTemplateRecord(&$params, TemplateService $parentObject)
    {
        // Disable the inclusion of default TypoScript set via TYPO3_CONF_VARS
        //$parameters['isDefaultTypoScriptAdded'] = true;
        // Disable the inclusion of ext_typoscript_setup.txt of all extensions
        //$parameters['processExtensionStatics'] = false;

        // No template was found in rootline so far, so a custom "fake" sys_template record is added
        if ($parentObject->outermostRootlineIndexWithTemplate === 0) {
            if (is_array($params['rootLine']) && !empty($params['rootLine'])) {
                $rootPage = $params['rootLine'][0];
                $rootPageId = (int) $rootPage['uid'];
                $row = [
                    'uid'              => 1,
                    'pid'              => $rootPageId,
                    'config'           => '',
                    'constants'        => '',
                    'nextLevel'        => 0,
                    'static_file_mode' => 3,
                    'title'            => 'Fake Template',
                    'sitetitle'        => '',
                ];
                $parentObject->processTemplate($row, 'sys_' . $row['uid'], $rootPageId, 'sys_' . $row['uid']);
            }
        }
        if (!$parentObject->rootId) {
            $parentObject->rootId = 1;
        }
    }
}
