<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Error;

use GrossbergerGeorg\JsonContent\Helper\JsonSerializeTrait;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Error\DebugExceptionHandler;
use TYPO3\CMS\Core\Utility\HttpUtility;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ExceptionHandler extends DebugExceptionHandler
{
    use JsonSerializeTrait;

    public function echoExceptionWeb(\Throwable $exception)
    {
        header(HttpUtility::HTTP_STATUS_500);
        header('Content-Type: application/json;charset=UTF-8');
        $result = [
            'message' => 'An error occured',
            'error'   => $exception->getCode(),
        ];

        if (!Environment::getContext()->isProduction()) {
            $result['exception'] = [
                'message' => $exception->getMessage(),
                'file'    => $exception->getFile(),
                'line'    => $exception->getLine(),
                'trace'   => $exception->getTrace(),
            ];
        }

        echo $this->serialize($result);
        exit();
    }
}
