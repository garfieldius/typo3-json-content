<?php
declare(strict_types=1);

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

namespace GrossbergerGeorg\JsonContent\Error;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use TYPO3\CMS\Core\Core\Environment;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Http\RedirectResponse;

/**
 * @author Georg Großberger <contact@grossberger-ge.org>
 */
class ExceptionCatchingMiddleware implements LoggerAwareInterface, MiddlewareInterface
{
    use LoggerAwareTrait;

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        try {
            $response = $handler->handle($request);
            $contentType = implode(' ', $response->getHeader('Content-Type'));

            if (!$response instanceof RedirectResponse
                && !$response instanceof JsonResponse
                && stripos($contentType, 'application/json') === false) {
                $this->logger->error('No JSON content type header in response', [$response]);
                $content = $response->getBody()->getContents();

                if ($content && strpos($content, '<title>') !== false) {
                    $pos = strpos($content, '<title>') + 7;
                    $title = html_entity_decode(substr($content, $pos, strpos($content, '<', $pos) - $pos));

                    $jsonResponse = new JsonResponse(['error' => true, 'message' => $title], 500);

                    if ($response->getStatusCode() > 206) {
                        $jsonResponse = $jsonResponse->withStatus($response->getStatusCode());
                    }

                    return $jsonResponse;
                }

                throw new \ErrorException('No JSON response');
            }

            return $response;
        } catch (\Throwable $ex) {
            $this->logger->critical('Uncaught error', [$ex]);

            if (Environment::getContext()->isDevelopment()) {
                $message = [
                    'error'   => true,
                    'message' => $ex->getMessage(),
                    'code'    => $ex->getCode(),
                    'file'    => $ex->getFile(),
                    'line'    => $ex->getLine(),
                    'trace'   => $ex->getTrace(),
                ];
            } else {
                $message = ['error' => true, 'message' => 'Unknown error'];
            }

            return new JsonResponse($message, 500);
        }
    }
}
