<?php

/*
 * (c) 2020 Georg Großberger <contact@grossberger-ge.org>
 *
 * This file is free software; you can redistribute it and/or
 * modify it under the terms of the Apache License 2.0
 *
 * For the full copyright and license information see
 * <https://www.apache.org/licenses/LICENSE-2.0>
 */

$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][\TYPO3\CMS\Backend\Form\Element\ImageManipulationElement::class]['className'] =
    \GrossbergerGeorg\BaseMinimal\CropVariants\ExtendedVariantsAwareManipulationElement::class;

$GLOBALS['TYPO3_CONF_VARS']['FE']['ContentObjects']['JSON_OBJECT'] =
    \GrossbergerGeorg\JsonContent\Rendering\JsonObject::class;

$GLOBALS['TYPO3_CONF_VARS']['SYS']['productionExceptionHandler'] =
$GLOBALS['TYPO3_CONF_VARS']['SYS']['debugExceptionHandler'] =
    \GrossbergerGeorg\JsonContent\Error\ExceptionHandler::class;

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['Core/TypoScript/TemplateService']['runThroughTemplatesPostProcessing'][1587828061] =
    \GrossbergerGeorg\JsonContent\Hooks\TyposcriptTemplateHook::class . '->addFakeTemplateRecord';
