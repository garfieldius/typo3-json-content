
.PHONY: build
build: vendor/autoload.php

.PHONY: test
test: bin/phpunit
	@bin/phpunit -c phpunit.xml

.PHONY: fix
fix: bin/php-cs-fixer
	@PHP_CS_FIXER_FUTURE_MODE=1 bin/php-cs-fixer fix --config=.php_cs --verbose

.PHONY: lint
lint: bin/php-cs-fixer
	@PHP_CS_FIXER_FUTURE_MODE=1 bin/php-cs-fixer fix --config=.php_cs --verbose --dry-run --diff

vendor/autoload.php bin/php-cs-fixer bin/phpunit: composer.json
	@composer install -n --ansi --no-suggest --no-progress
	@touch vendor/autoload.php bin/php-cs-fixer bin/phpunit
